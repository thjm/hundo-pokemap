#!/usr/bin/env python3
#
# some links:
#  - https://medium.com/future-vision/google-maps-in-python-part-2-393f96196eaf
#  - https://developers.google.com/maps/documentation/geocoding/get-api-key
#  - https://blog.goodaudience.com/geo-libraries-in-python-plotting-current-fires-bffef9fe3fb7
#  - https://geopandas.readthedocs.io/en/latest/gallery/plotting_basemap_background.html
#  - https://developers.arcgis.com/python/sample-notebooks/openstreetmap-exploration/
#  - https://www.geeksforgeeks.org/python-plotting-google-map-using-gmplot-package/
#
"""
Create something from 100 IV Pokemon coordinates.
"""

import sys
from datetime import datetime

import numpy as np
import pandas as pd

# for parsing command line options
from argparse import ArgumentParser

# columns of our future dataframe
#DF_COLUMNS = ['Name', 'Level', 'Lat', 'Lon', 'City', 'Start', 'End', 'Duration']
DF_COLUMNS = ['Name', 'Level', 'Lat', 'Lon', 'City', 'Duration', 'Date' , 'Timestamp']


# dictionaries with IV values, spawn locations and times
from iv100_oct import iv100_oct
from iv100_nov import iv100_nov
from iv100_dec import iv100_dec
from iv100_jan import iv100_jan
from iv100_feb import iv100_feb
from iv100_mar import iv100_mar

# this will work with Python 3.9 or greater
#iv100_dict = iv100_oct | iv100_nov
iv100_dict = { **iv100_oct, **iv100_nov, **iv100_dec, **iv100_jan, **iv100_feb, **iv100_mar }

# gym locations
lie_gyms = [(49.166050, 8.427350), (49.163988, 8.423774), (49.166071, 8.416810), (49.163050, 8.414950),
            (49.161700, 8.417760), (49.162530, 8.405290), (49.158652, 8.415235), (49.157640, 8.420294),
            (49.157067, 8.420356),]
lie_stops = [(49.165079, 8.426472), (49.167454, 8.416713), (49.160902, 8.426489), (49.163293, 8.421017),
             (49.162545, 8.419763), (49.161408, 8.421120), (49.163342, 8.418154), (49.163170, 8.415436),
             (49.163811, 8.416883), (49.163170, 8.415436), (49.162833, 8.415146), (49.162489, 8.416034),
             (49.161338, 8.418260), (49.162353, 8.413968), (49.162400, 8.414312), (49.162353, 8.413968),
             (49.162297, 8.413645), (49.162328, 8.412462), (49.162299, 8.410871), (49.163231, 8.403279), 
             #(49.166122, 8.400102),
             (49.158931, 8.408170), (49.164922, 8.413074), (49.159971, 8.416726), (49.159397, 8.416536),
             (49.158293, 8.414494), (49.158101, 8.415593), (49.157738, 8.416640), (49.157382, 8.417549),
             (49.156754, 8.419240), (49.158440, 8.418700), (49.158012, 8.419054), (49.156459, 8.416625),
             (49.155161, 8.414872),]
             #(49.150209, 8.422663),

# setup a nice user interface in unix style

parser = ArgumentParser(description=__doc__)

parser.add_argument('-d', '--debug', default=0, type=int,
                          help='set debug level')
parser.add_argument('-c', '--city', choices=['gbn', 'lie', 'lin', 'ndf'], default='lie', type=str,
                    help='select city')
parser.add_argument('-p', '--plot', choices=['all','test'], default='single', type=str,
                    help='select what to plot')
parser.add_argument('--show-gyms', default=False, action='store_true',
					help='display optionally known gym positions on the map')
parser.add_argument('--show-stops', default=False, action='store_true',
					help='display optionally known pokestop positions on the map')

args = parser.parse_args()

# do the work ...

df = pd.DataFrame(columns=DF_COLUMNS)

index = 0

# create dataframe from info in dictionaries
for key, value in iv100_dict.items():
    if not len(value):
        continue
    if args.debug:
        print(f'Table contains {len(value)} entries for {key}')
    for ele in value:
        #print(ele)
        # skip empty rows, might occur during testing
        if not len(ele):
            continue
        try:
            # skip incomplete rows
            if abs(ele[2])< 0.01 or abs(ele[3]) < 0.01:
                continue
            dt1 = datetime(1970, 1, 1)
            dt1 = dt1.strptime('%s %s' % (key, ele[5]), '%d.%m.%y %H:%M')
            dt2 = datetime(1970, 1, 1)
            dt2 = dt2.strptime('%s %s' % (key, ele[6]), '%d.%m.%y %H:%M')
            v2a = {'Name' : ele[0],
                   'Level' : int(ele[1].split('L')[1]),
                   'Lat' : float(ele[2]),
                   'Lon' : float(ele[3]),
                   'City' : ele[4].lower(),
                   'Duration' : ele[7],
                   'Date' : key,
                   'begintime' : dt1,
                   'endtime' : dt2 }
            #r2a = pd.Series(v2a, name=value[0][0])
            r2a = pd.Series(v2a, name=f'{index}')
            df = df.append(r2a)
            index = index + 1
        except Exception as ex:
            print(ex)
            print(ele)

# ensure df['*time'] is a Series with dtype datetime64[ns]
df['begintime'] = pd.to_datetime(df['begintime'])
df['endtime'] = pd.to_datetime(df['endtime'])

#print(df)
#sys.exit()

# plot the coordinates
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as mpath
import matplotlib.patches as mpatches

def plot_polygon_patch(ax, points=[], facecolor='b', alpha=0.15):
    """
    Plot a partly transparent polygon patch with ...

    See also:
     https://matplotlib.org/examples/shapes_and_collections/path_patch_demo.html
    """
    if not points:
        return

    Path = mpath.Path

    path_data = []
    path_data.append((Path.MOVETO, (points[0][0], points[0][1])))
    for x, y in points[1:]:
        path_data.append((Path.LINETO, (x, y)))
    path_data.append((Path.LINETO, (points[0][0], points[0][1])))

    codes, verts = zip(*path_data)
    path = mpath.Path(verts, codes)
    patch = mpatches.PathPatch(path, facecolor=facecolor, alpha=alpha)

    ax.add_patch(patch)


if args.plot == 'single':
    fig1 = plt.figure(figsize=(12, 10))
    ax1 = fig1.add_subplot(111)

    # greater or equal than the start date and smaller than the end date
    oct2020 = (df['begintime'] >= '2020-10-01') & (df['begintime'] <= '2020-10-31')
    nov2020 = (df['begintime'] >= '2020-11-01') & (df['begintime'] <= '2020-11-30')
    dec2020 = (df['begintime'] >= '2020-12-01') & (df['begintime'] <= '2020-12-31')

    # mask out specific city
    city = df.City == args.city
    #city = (df.City == args.city) & (df.Duration >= 10) # longer spawn time
    ax1.plot(df[city & oct2020].Lon, df[city & oct2020].Lat, 'bx', label='Oct 2020', markersize=3)
    ax1.plot(df[city & nov2020].Lon, df[city & nov2020].Lat, 'rx', label='Nov 2020', markersize=3)
    ax1.plot(df[city & dec2020].Lon, df[city & dec2020].Lat, 'gx', label='Dec 2020', markersize=3)

    # home spot
    if args.city.lower() == 'lie':
        ax1.plot([8.422540], [49.163700], 'ko', markersize=4.0, label='Home')
    # plot gyms
    if args.city.lower() == 'lie' and args.show_gyms:
        marker_lat = [x[0] for x in lie_gyms]
        marker_lon = [x[1] for x in lie_gyms]
        #ax1.scatter(marker_lon, marker_lat, 'bo')
        ax1.plot(marker_lon, marker_lat, 'mo', markersize=3.0, label='Gyms')
    # plot stops
    if args.city.lower() == 'lie' and args.show_stops:
        marker_lat = [x[0] for x in lie_stops]
        marker_lon = [x[1] for x in lie_stops]
        ax1.plot(marker_lon, marker_lat, 'co', markersize=3.0, label='Stops')

    # test: search for duplicated rows, subset of columns
    # 'By setting keep on False, all duplicates are True'
    dupes = df[city].duplicated(subset=['Lon', 'Lat'], keep=False)
    n_dupes = df[city].duplicated(subset=['Lon', 'Lat'], keep='first').sum()
    print(f'Number of dupe locations: {n_dupes}')
    #print(dupes)
    ax1.plot(df[city & dupes].Lon, df[city & dupes].Lat, 'ks', label='Dupes', markersize=3)

    ax1.set_xlabel('Longitude [degree]', fontsize=16)
    ax1.set_ylabel('Latitude [degree]', fontsize=16)
    # todo: single plot title ?
    plt.title('Hundo locations, %s (%d)' % (args.city.upper(),len(df[city])), fontsize=18)
    plt.legend(numpoints=1, loc='best')

if args.plot == 'all':
    colormap = { 'gbn' : 'r', 'lie' : 'b', 'ndf' : 'g', 'lin' : 'm' }

    fig2 = plt.figure(figsize=(12, 10))
    ax2 = fig2.add_subplot(111)

    # cities with different colors
    gbn = df.City == 'gbn'
    ax2.plot(df[gbn].Lon, df[gbn].Lat, '%sx' % colormap['gbn'], label='Gbn (%d)' % len(df[gbn]), markersize=3)
    lie = df.City == 'lie'
    ax2.plot(df[lie].Lon, df[lie].Lat, '%sx' % colormap['lie'], label='Lie (%d)' % len(df[lie]), markersize=3)
    #lin = df.City == 'lin'
    #ax2.plot(df[lin].Lon, df[lin].Lat, '%sx' % colormap['lin'], label='Lin (%d)' % len(df[lin]), markersize=3)
    ndf = df.City == 'ndf'
    ax2.plot(df[ndf].Lon, df[ndf].Lat, '%sx' % colormap['ndf'], label='Ndf (%d)' % len(df[ndf]), markersize=3)

    ax2.set_xlabel('Longitude [degree]', fontsize=16)
    ax2.set_ylabel('Latitude [degree]', fontsize=16)

    gbn_points = [(8.485, 49.148), (8.500, 49.157), (8.500, 49.165),
                  (8.486, 49.169), (8.470, 49.158), (8.470, 49.154)]
    plot_polygon_patch(ax2, points=gbn_points, facecolor=colormap['gbn'])
    lie_points = [(8.416, 49.156), (8.422, 49.156), (8.428, 49.160),
                  (8.428, 49.168),
                  (8.406, 49.168), (8.404, 49.162)]
    plot_polygon_patch(ax2, points=lie_points, facecolor=colormap['lie'])
    ndf_points = [(8.493, 49.168), (8.500, 49.166), (8.507, 49.170),
                  (8.507, 49.178), (8.495, 49.184), (8.480, 49.184),
                  (8.480, 49.180), (8.493, 49.175)]
    plot_polygon_patch(ax2, points=ndf_points, facecolor=colormap['ndf'])

    plt.legend(numpoints=1, loc='best')

    # city 'gbn': 8.470 ... 8.500, 49.150 ... 49.170
    # city 'lie': 8.405 ... 8.430, 49.156 ... 49.168
    # city 'lin': 
    # city 'ndf': 8.480 ... 8.505, 49.160 ... 49.190

if args.plot == 'test':

    #print(df)
    #print(df['Date'].count())
    #df = df.set_index('begintime').resample('D')['Level'].mean()
    #df.set_index('begintime').groupby(pd.Grouper(freq='D')).mean()
    #df = df.resample('D', on='begintime').mean()
    ###
    city = df.City == 'lie'
    df = df[city]
    print(df)
    df = df.groupby([df['Date']])['City'].count()
    #df = df.set_index('Date').resample('D')['City'].count()
    print(df)
    #df['date_object'] = datetime.strptime(df['Date'], '%m.%d.%y').date()
    #print(df)

    #fig1 = plt.figure(figsize=(12, 10))
    #ax1 = fig1.add_subplot(111)
    #ax1.plot(df[1], df[0])

plt.show()

# eof
